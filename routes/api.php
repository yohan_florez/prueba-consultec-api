<?php

use Illuminate\Http\Request;

Route::group([ 'prefix' => 'v1'], function () {
	Route::resource('city', 'v1\UtilController');
	Route::resource('client', 'v1\ClientController');
	Route::put('update-client', 'v1\ClientController@update');
	Route::post('delete-inactive', 'v1\ClientController@deleteInactive');
	Route::get('get-state', 'v1\UtilController@getState');
	Route::get('location', 'v1\LocationController@index');
	Route::post('location', 'v1\LocationController@store');
});
