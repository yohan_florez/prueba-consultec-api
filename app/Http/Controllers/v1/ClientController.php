<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Client;
use App\City;
use App\Status;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Client::with('city', 'status')->where('id_state', 1)->orderBy('id', 'DESC')->get();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function deleteInactive(Request $request)
    {
        $data = Client::find($request->id_client);
        if($data){
            $data->id_state = $request->status;
            if($data->save()){
                    return ['status' => true, 'data' => $data ];
                }else
                    return ['status' => false];
        }else
            return ['error-id' => false];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'cc' => 'required',
            'id_state' => 'required',
            'id_city' => 'required'
        ]);
        if($validator->fails())
            return response($validator->errors(), 400);
    	$data = new Client();
        $data->name = $request->name;
        $data->last_name = $request->last_name;
        $data->cc = $request->cc;
        $data->id_state = $request->id_state;
        $data->id_city = $request->id_city;
        if($data->save()){
            return ['status' => true, 'data' => $data ];
        }else
            return ['status' => false];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Client::find($request->id);
        if($data){
            $data->name = $request->name;
            $data->last_name = $request->last_name;
            $data->cc = $request->cc;
            $data->id_state = $request->id_state;
            $data->id_city = $request->id_city;
            if($data->save()){
                    return ['status' => true, 'data' => $data ];
                }else
                    return ['status' => false];
        }else
            return ['error-id' => false];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
